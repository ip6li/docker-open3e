FROM debian:bookworm
LABEL maintainer="Christian Felsing <support@ip6.li>"

# Install dependencies
RUN apt update && apt -y full-upgrade && DEBIAN_FRONTEND=noninteractive apt install -y \
  apt-utils \
  locales

RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

RUN DEBIAN_FRONTEND=noninteractive apt install -y \
  python3 \
  python3-pip

RUN DEBIAN_FRONTEND=noninteractive apt install -y \
  git

RUN \
  mkdir -p /opt; cd /opt && \
  git clone --branch=develop --depth=1 https://github.com/abnoname/open3e.git open3e && \
  cd open3e && \
  pip3 install -r requirements.txt --break-system-packages

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod ugo+rx /entrypoint.sh

entrypoint [ "/entrypoint.sh" ]

